'''
Description:
Sentiment analysis of election tweets and making bar plots from the resulting pandas DataFrame
'''


import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
import statsmodels.formula.api as smf
import statsmodels.api as sm
import json
from textblob import TextBlob as tb

def get_sentiment(fname):
    ''' This function takes a filename as its sole argument.  The file contains a jsonstring representing a list of
    the text of a number of tweets.  Open the file in read mode and use the jsonmodule to load its contents into a 
    Python list.  Use textblob to perform sentiment analysis on each of the tweets, rejecting any that have both 
    polarity and subjectivity equal to 0.0
    fname: parameter representing a JSON file
    Return a list containing mean of polarities and the standard deviation
    '''
    with open(fname) as fp:
        new = json.load(fp)
    blob = [tb(tweet) for tweet in new]
    polar = pd.Series([blob1.polarity for blob1 in blob if not (blob1.polarity == 0.0 and blob1.subjectivity == 0.0)])
    mean = sum(polar)/len(polar)
    std = polar.std(ddof=1)
    return [mean,std]
    
def get_ct_sentiment_frame():
    '''This function creates a dataframe of the pre/post means and standard deviations for Clinton and Trup
    No parameters
    Returns a dataframe
    '''
    clint_pre = get_sentiment('clinton_tweets_pre.json')
    clint_post = get_sentiment('clinton_tweets_post.json')
    clinton = clint_pre+clint_post
    trump_pre = get_sentiment('trump_tweets_pre.json')
    trump_post = get_sentiment('trump_tweets_post.json')
    trump = trump_pre + trump_post
    index = ['Clinton','Trump']
    data = [clinton,trump]
    columns = ['pre_mean','pre_std','post_mean','post_std']
    return pd.DataFrame(data, index=index, columns=columns)

def make_fig(sent_frame):
    '''This function takes the sentiment dataframe and creates a bar chart representing the data.
    sent_frame: parameter representing the dataframe 
    Returns None
    '''
    data = sent_frame[['pre_mean','post_mean']].T
    yerr = sent_frame[['pre_std','post_std']].T
    data.index = yerr.index = ['Clinton-Trump pre','Clinton-Trump post']
    width = .5
    ax = data.plot.bar(rot=0,yerr=yerr)
    ax.set_ylabel('Sentiment')
    ax.legend().set_visible(False)
#==========================================================
def main():
    '''put input function call in main'''
    df = get_ct_sentiment_frame()
    make_fig(df)
    input('enter to end')
if __name__ == '__main__':
    main()
